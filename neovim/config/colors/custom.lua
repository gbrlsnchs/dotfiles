package.loaded["lib.colorscheme"] = nil

local lush = require("lush")
local my_colorscheme = require("lib.colorscheme")

lush(my_colorscheme)
